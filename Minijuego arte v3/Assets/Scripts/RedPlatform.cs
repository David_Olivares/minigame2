using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RedPlatform : MonoBehaviour
{
    private bool down = false;
    private float speed = 0.5f;
    [SerializeField] private Transform padre;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (down)
        {
            StartCoroutine("Shocking");
            
            //Destroy(gameObject.GetComponent<Plaf);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.gameObject.name);
        down = true;
    }
    IEnumerator Shocking()
    {
        yield return new WaitForSeconds(2f);
        GoDown();

    }
    private void GoDown()
    {
        padre.Translate(Vector3.down * speed * Time.deltaTime);
    }
}
