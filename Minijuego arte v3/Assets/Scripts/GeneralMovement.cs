using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GeneralMovement : MonoBehaviour
{
    [SerializeField]private float speed = 5;
    
    public bool start = false;
    public bool move = false;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine("StartMovement");
    }
    private void Update()
    {
        if (move)
        {
            transform.Translate(Vector3.right * speed * Time.deltaTime);
        }
    }

    IEnumerator StartMovement()
    {
        yield return new WaitForSeconds(10f);
        move = true;
    }
}
