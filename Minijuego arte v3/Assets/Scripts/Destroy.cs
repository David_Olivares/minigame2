using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Destroy : MonoBehaviour
{
    [SerializeField]
    private GameObject acabo;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        Destroy(other.gameObject);
        StartCoroutine("SeAcabo");
    }
    IEnumerator SeAcabo()
    {
        acabo.SetActive(true);
        Time.timeScale = 0.2f;
        yield return new WaitForSeconds(1f);
        SceneManager.LoadScene(1);
    }
}
